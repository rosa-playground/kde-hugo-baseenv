# KDE Hugo Containerized Environment Base

This repository contains files for constructing a common base environment for building KDE-style Hugo sites in [Red Hat OpenShift Service on AWS](https://www.redhat.com/en/technologies/cloud-computing/openshift/aws) clusters.

## Licensing

These files are licensed under the Apache Software License, version 2.0. See `LICENSE` for details.
